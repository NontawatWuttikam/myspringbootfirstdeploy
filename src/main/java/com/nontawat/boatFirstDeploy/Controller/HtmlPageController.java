package com.nontawat.boatFirstDeploy.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;


@RestController
@RequestMapping("html")
public class HtmlPageController {

    @GetMapping(value="/Ricardo")
    public String getMethodName() {
        String html = "<body><img src=\"https://i.pinimg.com/originals/6c/69/47/6c694726c00d5b526790878676273aab.gif\">"+
                        "<p>Boat's first springboot api deployment</p></body>"+
                        "<style>body{background-color: rgb(28,21,48);} p{color: white}</stype>";
        return html;
    }
    
}