package com.nontawat.boatFirstDeploy.Controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Optional;

import javax.validation.constraints.NotNull;

import com.nontawat.boatFirstDeploy.Model.Todo;
import com.nontawat.boatFirstDeploy.Repository.TodoRepository;
import com.nontawat.boatFirstDeploy.Handler.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;




@RestController
@RequestMapping(value = "todaytodo")
public class getTodayTodoController {

    @Autowired
    private TodoRepository todoRepository;

    @GetMapping(value={"","/"})
    public ResponseEntity<List<Todo>> getAll(){
        return ResponseEntity.status(HttpStatus.OK).body(todoRepository.getTodos());
    }

   @PostMapping(value={"","/"})
   public ResponseEntity<String> insertTodo(
       @RequestBody @NotNull Optional<Todo> opTodo
   ) {  
       if(!opTodo.isEmpty()) {
            todoRepository.insertTodo(opTodo.get());
        }
        else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(MessageHandler.INSERT_BODY_INVALID);
        }
        return ResponseEntity.status(HttpStatus.OK).body(MessageHandler.INSERT_SUCCESS);
   }
   
    
}