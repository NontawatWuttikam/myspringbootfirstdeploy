package com.nontawat.boatFirstDeploy.Model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component("Todo")
public class Todo {

    private String title;
    private String description;
    private Date date;
    private Long eventId;

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }
    
    public Long getEventId() {
        return this.eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return this.date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
    
}