package com.nontawat.boatFirstDeploy.Repository;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.nontawat.boatFirstDeploy.Model.Todo;

import org.springframework.stereotype.Repository;

@Repository
public class TodoRepository {
    
    private Map<Long,Todo> todoList = new HashMap<>();
    public void insertTodo(Todo todo) {
        todoList.put(todo.getEventId(), todo);
    }

    public void updateTodo(Long id,Todo todo) throws Exception {
        if(todoList.containsKey(id)){
            todoList.put(todo.getEventId(), todo);
        }
        else
            throw new Exception();
    }
    public void deleteTodo(Long id) throws Exception {
        if(todoList.containsKey(id)) {
            todoList.remove(id);
        }
        else
            throw new Exception("todo key doesnt exist");
    }
    public List<Todo> getTodos() {
        if(todoList.isEmpty()){
            Todo todo = new Todo();
            todo.setDate(new Date());
            todo.setDescription("description");
            todo.setEventId(1L);
            todo.setTitle("API Test");
            todoList.put(todo.getEventId(), todo);
        }
        return new ArrayList<>(this.todoList.values());
    }
}