package com.nontawat.boatFirstDeploy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
@ComponentScan
public class BoatFirstDeployApplication {

	@RequestMapping(value = "/api",method = RequestMethod.GET)
	public ResponseEntity<String> getApiExample() {
		return ResponseEntity.status(HttpStatus.OK).body("This is Boat's First deploy app!!");
	}
	@GetMapping(value = "/api2")
	public ResponseEntity<String> getApiExample2() {
		return ResponseEntity.status(HttpStatus.OK).body("This is to test update deployment!!");
	}
	public static void main(String[] args) {
		SpringApplication.run(BoatFirstDeployApplication.class, args);
	}

}
