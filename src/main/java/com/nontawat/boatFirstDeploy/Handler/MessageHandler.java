package com.nontawat.boatFirstDeploy.Handler;

import org.springframework.stereotype.Component;

@Component("MessageHandler")
public class MessageHandler {

    public static String INSERT_SUCCESS = "-insert successfully";
    public static String INQUIRY_SUCCESS = "-Get successfully-";
    public static String INSERT_BODY_INVALID = "-Body is invalid- JSON body is null or invalid";
    
}